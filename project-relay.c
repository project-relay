/*
 * Relay server with metadata injection for Project Reloaded Shoutcast stream.
 * See www.project-reload.com for more on this station (in German).
 *
 * Copyright (C) 2009 Jan Kiszka <jan.kiszka@web.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <getopt.h>
#include <libgen.h>
#include <netdb.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>

#define WRITE_ISO8859_1		1

#define MIN(a,b)		((a) < (b) ? (a) : (b))

#define ERR_FATAL		1
#define ERR_CONN		2

#define DEFAULT_RELAY_PORT	8010

#define USER_AGENT_AND_ACCEPT 	"User-Agent: project-relay " VERSION "\r\n" \
				"Accept: */*\r\n"

#ifndef USE_OLD_SERVER
#define PROJECT_PORT		80
#define PROJECT_SERVER		"edge.live.mp3.mdn.newmedia.nacamar.net"
#define PROJECT_PATH		"/projectreloaded/livestream.mp3"
#else
#define PROJECT_PORT		8010
#define PROJECT_SERVER		"62.26.4.172"
#define PROJECT_PATH		"/"
#endif

#define PLAYLIST_PORT		80
#define PLAYLIST_SERVER		"www.project-reloaded.com"

#define PLAYLIST_MAIN		0
#define PLAYLIST_PREVIEW	1

static const char * const playlist_path[2] = {
	[PLAYLIST_MAIN]    = "/widget/radio/get_playlist.php",
	[PLAYLIST_PREVIEW] = "/widget/radio/preview_playlist.php"
};

#define check_error(class, error) \
	do_check_error(class, error, 0, __FUNCTION__, __LINE__)

#define check_error_and_data(class, error) \
	do_check_error(class, error, 1, __FUNCTION__, __LINE__)

static int debug;
static int metadata_socket = -1;
static int watched_socket;

static void dbgprintf(const char *fmt, ...)
{
	va_list ap;

	if (debug) {
		va_start(ap, fmt);

		vprintf(fmt, ap);
		fflush(stdout);
		va_end(ap);
	}
}

static int do_check_error(int class, int error, int check_data,
			  const char *function, int line)
{
	if (check_data && error == 0)
		errno = ENODATA;
	else if (error >= 0)
		return 0;

	if (class == ERR_FATAL) {
		fprintf(stderr, "[%s:%d] FATAL: %s\n", function, line,
			strerror(errno));
		exit(1);
	}
	dbgprintf("[%s:%d] %s\n", function, line, strerror(errno));

	return 1;
}

static void decapitalize(char *str, size_t size, int iso8859_1_out)
{
	int utf_escape = 0;
	int in_word = 0;
	char *dst = str;
	uint8_t c;

	while ((c = *str) && size-- > 1) {
		if (c == 0xc3)
			utf_escape = 1;
		else if (utf_escape) {
			utf_escape = 0;
			if (iso8859_1_out)
				c += in_word ? 0x60 : 0x40;
			in_word = 1;
		} else if (c >= 'A' && c <= 'Z') {
			if (in_word)
				c = tolower(c);
			in_word = 1;
		} else
			switch (c) {
			case 0xc4:
			case 0xd5:
			case 0xdc:
				if (iso8859_1_out) {
					if (in_word)
						c += 0x20;
					in_word = 1;
				} else {
					if (size-- < 3)
						goto out;
					*dst++ = 0xc3;
					c -= in_word ? 0x20 : 0x40;
				}
				break;
			case '\'':
				break;
			default:
				in_word = 0;
				break;
			}

		*dst = c;
		if (!utf_escape || !iso8859_1_out)
			dst++;
		str++;
	}
out:
	*dst = 0;
}

static int resolved_host(const char *hostname, struct in_addr *paddr)
{
	struct hostent *host;

	host = gethostbyname(hostname);
	if (!host) {
		dbgprintf("resolve_host(%s): %s\n",
			  hostname, strerror(h_errno));
		return -1;
	}

	paddr->s_addr = *(uint32_t *)host->h_addr_list[0];

	return 0;
}

static ssize_t read_http_reply(int srv, char *buf, size_t buf_size)
{
	ssize_t len = 0;
	ssize_t res;

	do {
		res = read(srv, buf, buf_size-1 - len);
		if (res <= 0)
			return res;
		len += res;
	} while (!strstr(buf, "\r\n\r\n") && len < buf_size-1);

	buf[buf_size-1] = 0;

	return len;
}

static void download_metadata(char *artist, char *next_artist,
			      size_t artist_size,
			      char *title, char *next_title,
			      size_t title_size, int playlist)
{
	const char request_head[] = "GET ";
	const char request_tail[] =
		" HTTP/1.1\r\n"
		"Host: " PLAYLIST_SERVER "\r\n"
		USER_AGENT_AND_ACCEPT
		"Connection: keep-alive\r\n"
		"Keep-Alive: 300\r\n"
		"Cache-Control: no-cache\r\n\r\n";
	struct sockaddr_in addr;
	char *artist_buf, *title_buf;
	char buf[4096];
	char fmt[128];
	int artist_tag;
	int target_tag;
	char *pos;
	ssize_t res;
	int i;

	dbgprintf("start reading metadata... ");

	artist[0] = next_artist[0] = 0;
	title[0] = next_title[0] = 0;

	if (metadata_socket >= 0) {
		if (recv(metadata_socket, NULL, 0, MSG_DONTWAIT) == 0) {
			close(metadata_socket);
			metadata_socket = -1;

			dbgprintf("connection expired... ");
		} else {
			watched_socket = metadata_socket;
			alarm(2);

			dbgprintf("reusing connection... ");
		}
	}

	if (metadata_socket < 0) {
		metadata_socket = socket(PF_INET, SOCK_STREAM, 0);

		addr.sin_family = AF_INET;
		addr.sin_port = htons(PLAYLIST_PORT);
		if (resolved_host(PLAYLIST_SERVER, &addr.sin_addr) < 0)
			goto cleanup_close;

		watched_socket = metadata_socket;
		alarm(2);

		res = connect(metadata_socket, (struct sockaddr *)&addr,
			      sizeof(addr));
		if (check_error(ERR_CONN, res))
			goto cleanup_close;

		dbgprintf("connected... ");
	}

	res = write(metadata_socket, request_head, strlen(request_head));
	if (check_error(ERR_CONN, res))
		goto cleanup_close;
	res = write(metadata_socket, playlist_path[playlist],
		    strlen(playlist_path[playlist]));
	if (check_error(ERR_CONN, res))
		goto cleanup_close;
	res = write(metadata_socket, request_tail, strlen(request_tail));
	if (check_error(ERR_CONN, res))
		goto cleanup_close;

	dbgprintf("awaiting reply... ");

	res = read_http_reply(metadata_socket, buf, sizeof(buf));
	if (check_error_and_data(ERR_CONN, res))
		goto cleanup_close;

	pos = buf;
	artist_tag = 0;
	target_tag = playlist == PLAYLIST_MAIN ? 1 : 4;
	artist_buf = artist;
	title_buf = title;

	for (i = 0; i < 2; i++) {
		do {
			pos = strstr(pos, "<artist>");
			if (!pos) {
				dbgprintf("Invalid playlist: slot %d not "
					  "found!\n", target_tag);
				goto cleanup_noclose;
			}
			pos += 8;
			artist_tag++;
		} while (artist_tag < target_tag);

		snprintf(fmt, sizeof(fmt),
			 " <![CDATA[%%%zu[^]]]]> </artist>"
			 " <title> <![CDATA[%%%zu[^]]]]> </title> </slot>",
			 artist_size-1, title_size-1);
		if (sscanf(pos, fmt, artist_buf, title_buf) < 2) {
			artist_buf[0] = 0;
			dbgprintf("Invalid playlist: slot parsing failed!\n");
			goto cleanup_noclose;
		}

		decapitalize(artist_buf, artist_size, WRITE_ISO8859_1);
		decapitalize(title_buf, title_size, WRITE_ISO8859_1);

		artist_buf = next_artist;
		title_buf = next_title;
		target_tag++;
	}

	dbgprintf("done\n");

cleanup_noclose:
	alarm(0);
	return;

cleanup_close:
	alarm(0);
	close(metadata_socket);
	metadata_socket = -1;
}

static ssize_t forward_data(int dst, int src, size_t len)
{
	uint8_t buf[len];
	ssize_t res;

	res = read(src, buf, len);
	if (check_error_and_data(ERR_CONN, res))
		return res;

	res = write(dst, buf, res);
	check_error(ERR_CONN, res);

	return res;
}

static ssize_t process_request_header(int conn, int stream)
{
	const char request[] =
		"GET " PROJECT_PATH " HTTP/1.1\r\n"
		"Host: " PROJECT_SERVER "\r\n"
		"Connection: keep-alive\r\n"
		USER_AGENT_AND_ACCEPT
		"Icy-Metadata:1\r\n\r\n";
	const char reject[] =
		"HTTP/1.0 400 Bad Request\r\n"
		"Content-Type: text/html\r\n";
	unsigned int metadata = 0;
	char buf[1024];
	char *tag;
	ssize_t len;

	len = read_http_reply(conn, buf, sizeof(buf));
	if (check_error_and_data(ERR_CONN, len))
		return -1;

	while (len > 0) {
		len--;
		buf[len] = tolower(buf[len]);
	}

	tag = strstr(buf, "icy-metadata:");
	if (tag)
		sscanf(tag, "icy-metadata: %ud\r\n", &metadata);
	if (metadata == 0) {
		dbgprintf("missing/invalid icy-metadata tag - disconnected\n");
		len = write(conn, reject, strlen(reject));
		return -1;
	}

	len = write(stream, request, strlen(request));
	check_error(ERR_CONN, len);

	return len;
}

static ssize_t process_reply_header(int conn, int stream, off_t *ppos)
{
	char buf[2048];
	ssize_t header_len = 0;
	ssize_t block_size;
	ssize_t res;
	char *pos;

	while (header_len < sizeof(buf)-1) {
		res = read(stream, &buf[header_len],
			   sizeof(buf)-1 - header_len);
		if (check_error_and_data(ERR_CONN, res))
			return -1;
		header_len += res;
	}
	buf[sizeof(buf)-1] = 0;

	block_size = 0;

	pos = buf;
	while (1) {
		sscanf(pos, "icy-metaint: %zd", &block_size);
		pos = strstr(pos, "\r\n");
		if (!pos || pos > &buf[sizeof(buf)-3]) {
			dbgprintf("Incomplete reply header!\n");
			return -1;
		}
		pos += 2;
		if (strncmp(pos, "\r\n", 2) == 0)
			break;
	}

	if (block_size == 0) {
		dbgprintf("Invalid/missin icy-metaint in reply header!\n");
		return -1;
	}

	*ppos = header_len - (pos + 2 - buf);
	dbgprintf("block size = %zu\n", block_size);

	res = write(conn, buf, header_len);
	if (check_error(ERR_CONN, res))
		return -1;

	return block_size;
}

static int inject_metadata(int conn, int stream, int new_connection)
{
	static int use_main_list = 0;
	static int metadata_errors;
	static int preview_list_errors;
	static time_t next_update;
	static char artist[64], next_artist[64];
	static char title[64], next_title[64];
	char new_artist[64], new_next_artist[64];
	char new_title[64], new_next_title[64];
	int new_metadata;
	char buf[128];
	uint8_t meta_len;
	size_t len;
	time_t now;
	int res;

	if (new_connection) {
		next_update = 0;
		artist[0] = 0;
		title[0] = 0;
		metadata_errors = 0;
		preview_list_errors = 0;
		/* allow early fall back to obtain some initial data */
		use_main_list = 1;
	}

	res = read(stream, &meta_len, 1);
	if (check_error_and_data(ERR_CONN, res))
		return -1;
	assert(res == 1);

	len = (size_t)meta_len * 16;
	while (len > 0) {
		res = read(stream, &buf, MIN(sizeof(buf), len));
		if (check_error_and_data(ERR_CONN, res))
			return -1;
		len -= res;
	}

	new_metadata = 0;
	now = time(NULL);
	if (now >= next_update) {
		next_update = now + 10;

		download_metadata(new_artist, new_next_artist,
				  sizeof(new_artist),
				  new_title, new_next_title,
				  sizeof(new_title), PLAYLIST_PREVIEW);
		if (new_artist[0]) {
			use_main_list = 0;
			preview_list_errors = 0;
		} else {
			preview_list_errors++;
			if (use_main_list) {
				dbgprintf("%spreview list outage, trying "
					  "main list\n",
					  new_connection ? "" : "longer ");
				download_metadata(new_artist, new_next_artist,
						  sizeof(new_artist),
						  new_title, new_next_title,
						  sizeof(new_title),
						  PLAYLIST_MAIN);
			}
		}
		if (new_artist[0])
			metadata_errors = 0;
		else
			metadata_errors++;

		dbgprintf("read metadata: \"%s\" - \"%s\", "
			  "then \"%s\" - \"%s\" (errors: total %d / "
			  "preview %d)\n",
			new_artist, new_title, new_next_artist, new_next_title,
			metadata_errors, preview_list_errors);

		if (metadata_errors == 0) {
			if (strcmp(new_artist, artist) ||
			    strcmp(new_title, title)) {
				if (artist[0] && !new_connection &&
				    (!use_main_list ||
				     preview_list_errors >= 40))
					next_update = now + 2.5 * 60;
				strcpy(artist, new_artist);
				strcpy(title, new_title);
				strcpy(next_artist, new_next_artist);
				strcpy(next_title, new_next_title);
				new_metadata = 1;
			}
		} else if (metadata_errors == 3) {
			dbgprintf("short playlist outage, using cached data: "
				  "\"%s\" - \"%s\"\n",
				  next_artist, next_title);
			strcpy(artist, next_artist);
			strcpy(title, next_title);
			new_metadata = 1;
		} else if (metadata_errors == 35) {
			use_main_list = 1;
		} else if (metadata_errors >= 40) {
			artist[0] = 0;
			title[0] = 0;
			new_metadata = 1;
			next_update = now + 2.5 * 60;
		}
	}

	if (new_metadata || new_connection) {
		dbgprintf("updating metadata\n");
		memset(buf, 0, sizeof(buf));
		if (artist[0])
			len = snprintf(buf+1, sizeof(buf)-1,
				       "StreamTitle='%s - %s';",
				       artist, title);
		else
			len = snprintf(buf+1, sizeof(buf)-1,
				       "StreamTitle='<%s not available>';",
				       use_main_list ? "both playlists":
						       "preview playlist");
		buf[0] = len / 16 + 1;
		res = write(conn, buf, 1 + buf[0] * 16);
	} else {
		res = write(conn, "\00", 1);
	}
	check_error(ERR_CONN, res);

	return res;
}

static void handle_connection(int conn)
{
	static const struct timeval timeout = { .tv_sec = 10, .tv_usec = 0 };
	struct sockaddr_in addr;
	int initialize = 1;
	ssize_t block_size;
	off_t block_pos;
	ssize_t count;
	int stream;
	int err;

	dbgprintf("new connection started\n");

	stream = socket(PF_INET, SOCK_STREAM, 0);

	if (setsockopt(stream, SOL_SOCKET, SO_RCVTIMEO,
		       &timeout, sizeof(timeout)) < 0 ||
	    setsockopt(stream, SOL_SOCKET, SO_SNDTIMEO,
		       &timeout, sizeof(timeout)) < 0 ||
	    setsockopt(conn, SOL_SOCKET, SO_RCVTIMEO,
		       &timeout, sizeof(timeout)) < 0 ||
	    setsockopt(conn, SOL_SOCKET, SO_SNDTIMEO,
		       &timeout, sizeof(timeout)) < 0)
		goto cleanup;

	addr.sin_family = AF_INET;
	addr.sin_port = htons(PROJECT_PORT);
	if (resolved_host(PROJECT_SERVER, &addr.sin_addr) < 0)
		goto cleanup;

	err = connect(stream, (struct sockaddr *)&addr, sizeof(addr));
	if (check_error(ERR_CONN, err))
		goto cleanup;

	if (process_request_header(conn, stream) < 0)
		goto cleanup;

	block_size = process_reply_header(conn, stream, &block_pos);
	if (block_size < 0)
		goto cleanup;

	while (1) {
		count = forward_data(conn, stream, block_size - block_pos);
		if (count < 0)
			break;

		block_pos += count;
		assert(block_pos <= block_size);
		if (block_pos == block_size) {
			if (inject_metadata(conn, stream, initialize) < 0)
				break;
			initialize = 0;
			block_pos = 0;
		}
	}

cleanup:
	close(stream);
	close(conn);
	if (metadata_socket >= 0) {
		close(metadata_socket);
		metadata_socket = -1;
	}
}

static void timeout_handler(int signal)
{
	dbgprintf("\ntimeout!\n");
	close(watched_socket);
}

static void usage(char *argv[])
{
	printf("Usage: %s [OPTIONS]\n\n"
	       "  -d, --debug\t\tstay in foreground and issue debug messages\n"
	       "  -p, --port PORT\tprovide radio stream on given TCP port "
			"(default: %d)\n"
	       "  -v, --version\t\tprint version and exit\n",
	       basename(argv[0]), DEFAULT_RELAY_PORT);
	exit(1);
}

int main(int argc, char *argv[])
{
	static const struct option options[] = {
		{ "help", 0, 0, 'h'},
		{ "version", 0, 0, 'v'},
		{ "port", required_argument, 0, 'p' },
		{ "debug", 0, 0, 'd'},
		{ 0, 0, 0, 0 }
	};
	int relay_port = DEFAULT_RELAY_PORT;
	struct sockaddr_in addr;
	int longindex;
	int srv, conn;
	int one = 1;
	int res;

	while (1) {
		res = getopt_long(argc, argv, "hvp:d", options, &longindex);
		if (res < 0)
			break;
		switch (res) {
		case 'v':
			printf("project-relay %s\n", VERSION);
			exit(0);
		case 'p':
			relay_port = atoi(optarg);
			break;
		case 'd':
			debug = 1;
			break;
		default:
			usage(argv);
			break;
		}
	}

	signal(SIGPIPE, SIG_IGN);
	signal(SIGALRM, timeout_handler);

	srv = socket(PF_INET, SOCK_STREAM, 0);
	setsockopt(srv, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one));

	addr.sin_family = AF_INET;
	addr.sin_port = htons(relay_port);
	addr.sin_addr.s_addr = INADDR_ANY;
	res = bind(srv, (struct sockaddr *)&addr, sizeof(addr));
	check_error(ERR_FATAL, res);

	listen(srv, 0);

	dbgprintf("project-relay %s, listening on port %d\n",
		  VERSION, relay_port);

	if (!debug) {
		res = daemon(0, 0);
		check_error(ERR_FATAL, res);
	}

	while (1) {
		conn = accept(srv, NULL, NULL);
		check_error(ERR_FATAL, conn);

		handle_connection(conn);
	}
}
