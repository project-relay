ifdef DEBUG
DEBUG_CFLAGS = -g -O0
endif

VERSION = $(shell git describe --tags)
ifneq ($(shell git diff --name-only HEAD 2>/dev/null),)
VERSION := $(VERSION)-dirty
endif
ifeq ($(VERSION),)
VERSION = v0.6
endif

CFLAGS = -O2 -Wall -DVERSION=\"$(VERSION)\" $(DEBUG_CFLAGS) $(EXTRA_CFLAGS)

all: project-relay

project-relay: project-relay.c

clean:
	rm -rf project-relay
